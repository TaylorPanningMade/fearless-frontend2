
window.addEventListener("DOMContentLoaded", async () => {
    // console.log('DOM content fully loaded and parsed.');

    const url = "http://localhost:8000/api/states/";

    // add dropdown list of states in form
    try {
        const response = await fetch(url);
        // console.log("Fectched URL")

        if (!response.ok) {
            console.log("Response is not okay")

        } else {
            const data = await response.json();
            // console.log(data);
        
            const selectTag = document.getElementById("state");
            for (let state of data.states) {
                let option = document.createElement("option");
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                selectTag.appendChild(option);
            }
        }   

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        // console.log('need to submit the form data');
    
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
        }
    });



    } catch(e) {
        console.log("error", e);
    }
});
    