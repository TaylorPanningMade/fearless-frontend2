function createCard(title, description, pictureUrl, newStartDate, newEndDate, location) {
    return `
    <div class="col">
    <div class="card shadow p-3 mb-5 bg-white rounded">
    <img src="${pictureUrl}" class="card-img-top">
    <div class="card-body">
    <h5 class="card-title">${title}</h5>
    <h6 class="card-subtitle mb-2 text-muted fs-9">${location}</h6>
    <p class="card-text">${description}</p>
    <div class="card-footer text-muted fs-9">
       ${newStartDate} - ${newEndDate}
    </div>
    </div>
    </div>
    </div>
    `;
  }

  //hidden is my attempt at the extra info
  /*
function placeholderCard(){
    return `
    <div class="card" aria-hidden="true">
          <img src="..." class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title placeholder-glow">
              <span class="placeholder col-6"></span>
            </h5>
            <p class="card-text placeholder-glow">
              <span class="placeholder col-7"></span>
              <span class="placeholder col-4"></span>
              <span class="placeholder col-4"></span>
              <span class="placeholder col-6"></span>
              <span class="placeholder col-8"></span>
            </p>
            <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
          </div>
        </div>
        `;
    }
 */ 


  function alert(message, type) {
  let alertPlaceholder = document.getElementById('liveAlertPlaceholder')
  let wrapper = document.createElement('div')
  wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
  alertPlaceholder.append(wrapper)
  }

  
  window.addEventListener('DOMContentLoaded', async () => {
      
      const url = 'http://localhost:8000/api/conferences/';
      
      try {
          const response = await fetch(url);
          
          if (!response.ok) {
              alert("You did it, but what", "danger");
        } else {
        
        const data = await response.json();
        
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              let startDate = details.conference.starts;
                let d1 = new Date(startDate);
                let newStartDate = (d1.getMonth()+1) + "/" + d1.getDate() + "/" + d1.getFullYear();
              const endDate = details.conference.ends;
                let d2 = new Date(endDate);
                let newEndDate = (d2.getMonth()+1) + "/" + d2.getDate() + "/" + d2.getFullYear();
              const location = details.conference.location.name;
              const html = createCard(title, description, pictureUrl, newStartDate, newEndDate, location);
              const column = document.querySelector('.row');
              column.innerHTML += html;
            }
          }
    
        }
      } catch (e) {
        // Figure out what to do if an error is raised
      }
    
    });
