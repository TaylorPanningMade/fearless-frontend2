window.addEventListener("DOMContentLoaded", async () => {
    // console.log('DOM content fully loaded and parsed.');

    
    // add dropdown list of states in form
    try {
        const urltwo = "http://localhost:8000/api/locations/";
        const response = await fetch(urltwo);
        // console.log("Fectched URL")

        if (!response.ok) {
            console.log("Response is not okay")

        } else {
            const data = await response.json();
            const selectTag = document.getElementById("location");
            for (let location of data.locations) {
                let option = document.createElement("option");
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
        
        }   

        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            // console.log('need to submit the form data');
            
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            
            const conferenceURL = "http://localhost:8000/api/conferences/";
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                }
            };
        const response = await fetch(conferenceURL, fetchConfig);
                //console.log(response);
        if (response.ok) {
          formTag.reset();
          const newConference = await response.json();
        }
    });



    } catch(e) {
        console.log("error", e);
    }
});