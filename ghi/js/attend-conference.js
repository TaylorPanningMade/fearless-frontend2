/*
You know from the JSON that you need the href value and 
not the id as the value for the option elements in the code. 
You change it so that option.value is the conference's href.
*/

window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      // Here, add the 'd-none' class to the loading icon
      const loadingTag = document.getElementById('loading-conference-spinner')
      loadingTag.classList.add('d-none')
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none')
    }
    
    const attendeeTag = document.getElementById('create-attendee-form');
    attendeeTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(attendeeTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const attendeeURL = "http://localhost:8001/api/attendees/";
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                }
            };
        const response = await fetch(attendeeURL, fetchConfig);
                //console.log(response);
        if (response.ok) {
            const successTag = document.getElementById('success-message');
            successTag.classList.remove('d-none');
            attendeeTag.classList.add('d-none');
          attendeeTag.reset();
          const newAttendee = await response.json();
        }
    })
  });
