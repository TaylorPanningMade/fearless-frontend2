
import React from "react";

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = { //need to have a default empty state for the case of 
            presenter_name: '',   //wanting to submit this later
            presenter_email: '',
            company_name: '',
            title:'',
            synopsis:'',
            conferences: [],
        };
        this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this);
        this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this);
        this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        this.handleConferenceChange = this.handleConferenceChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event){  //create a copy of the current state(status)
        event.preventDefault();
        const data = {...this.state};
        const iD = data.conference;
        console.log(iD);
        const idArray = iD.split('/');
        console.log(idArray);
        delete data.conferences
        const URL = `http://localhost:8000/api/conferences/${idArray[3]}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(URL, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            //console.log(newLocation);

            const cleared = {
                presenter_name: '',   //wanting to submit this later
                presenter_email: '',
                company_name: '',
                title:'',
                synopsis:'',
                conference: '',
            };
            this.setState(cleared);
        }
    }
    handlePresenterNameChange(event){
        const value = event.target.value;
        this.setState({presenter_name: value})
    }
    handlePresenterEmailChange(event){
        const value = event.target.value;
        this.setState({presenter_email: value})
    }
    handleCompanyNameChange(event){
        const value = event.target.value;
        this.setState({company_name: value})
    }
    handleTitleChange(event){
        const value = event.target.value;
        this.setState({title: value})
    }
    handleSynopsisChange(event){
        const value = event.target.value;
        this.setState({synopsis: value})
    }
    handleConferenceChange(event){
        const value = event.target.value;
        this.setState({conference: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
        this.setState({conferences: data.conferences});
        }
      }
    render() {
      return (
        <div className="container"> 
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Presentation</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handlePresenterNameChange} value={this.state.presenter_name} placeholder="presenter_name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                  <label htmlFor="presenter_name">Presenter Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handlePresenterEmailChange} value={this.state.presenter_email} placeholder="presenter_email" required type="text" name="presenter_email" id="presenter_email" className="form-control"/>
                  <label htmlFor="presenter_email">Presenter Email</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleCompanyNameChange} value={this.state.company_name} placeholder="company_name" required type="text" name="company_name" id="company_name" className="form-control"/>
                  <label htmlFor="company_name">Company Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleTitleChange} value={this.state.title} placeholder="title" required type="text" name="title" id="title" className="form-control"/>
                  <label htmlFor="title">Title</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleSynopsisChange} value={this.state.synopsis} placeholder="synopsis" required type="text" name="synopsis" id="synopsis" className="form-control"/>
                  <label htmlFor="synopsis">Synopsis</label>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleConferenceChange} value={this.state.conference} placeholder="conference" required name="conference" id="conference" className="form-select">
                    <option value="">Choose a conference</option>
                        {this.state.conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.href}>
                            {conference.name}
                            </option>
                            );
                        })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
        </div>
      );
    }
  }


export default PresentationForm;