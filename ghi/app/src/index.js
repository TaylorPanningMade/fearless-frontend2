import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// to install react router you need to run an install on the docker container
// that is running your front end services
// docker exec -it fearless-frontend-react-1
// npm install react-router-dom
//      double check that this lands in your package.json file under dependencies


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

reportWebVitals();

async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  if (response.ok) {
    const data = await response.json();
    //console.log(data);
      root.render(
        <React.StrictMode>
          <App attendees={data.attendees} />
        </React.StrictMode>
      );
  } else {
    console.error(response);
  }
}
loadAttendees();
