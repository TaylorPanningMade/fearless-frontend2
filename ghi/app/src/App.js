
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage';
import Nav from './Nav';
import React from "react";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeSignUpForm from './AttendeeSignUp';
import AttendConferenceForm from './AttendeeSignUp';
import PresentationForm from './PresentationForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
          <Routes>
            <Route>
              <Route index element={<MainPage />} />
            </Route>
            <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="presentations">
              <Route path="new" element={<PresentationForm />} />
            </Route>
            <Route path="Conferences">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="Attendees">
              <Route path="new" element={<AttendConferenceForm />} />
            </Route>
            <Route path="Attendees">
              <Route path="" element={<AttendeesList />} />
            </Route>
          </Routes>
     {/* <LocationForm /> */}
     {/* <AttendeesList attendees={props.attendees} /> */}
     {/*<ConferenceForm /> */} 
    {/* <AttendeeSignUpForm /> */} 
      
    </BrowserRouter>
  );
}

export default App;
